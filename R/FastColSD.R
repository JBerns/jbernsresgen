#' Standard Deviations of columns
#'
#' This function allows you to calculate the SD's of columns extremely fast (benchmarked with competing functions).
#' @param x Numeric vector.
#' @param na.rm Default is TRUE
#' @keywords Standard Deviations, Columns.
#' @export
#' @examples
#' A <- data.frame(runif(10))
#' A_SD <- colSdColMeans(A,na.rm=TRUE)
#' @references Source: https://stackoverflow.com/questions/17549762/is-there-such-colsd-in-r

colSdColMeans <- function(x, na.rm=TRUE) {
  if (na.rm) {
    n <- colSums(!is.na(x))
  } else {
    n <- nrow(x)
  }
  colVar <- colMeans(x*x, na.rm=na.rm) - (colMeans(x, na.rm=na.rm))^2
  return(sqrt(colVar * n/(n-1)))
}


