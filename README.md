# *R* Package for a random assortment of function

Includes the following custom functions:
- 
- colSdColMeans: Calculate the SD's of columns extremely fast
- my.max: Calculate the maximum value in a vector whilst ignoring NA's
- my.min: Calculate the minimum value in a vector whilst ignoring NA's
- Normalize: Normalize a variable using 'min-max normalization'
- moveMe: Move columns in a large dataframe using column names
